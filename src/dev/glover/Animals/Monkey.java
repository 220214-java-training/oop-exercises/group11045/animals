package dev.glover.Animals;

public class Monkey extends Mammal
{
    private float armLength;

    Monkey()
    {
    }

    Monkey(int i, float armLength)
    {
        super(i);
        setArmLength(armLength);
    }

    public float getArmLength()
    {
        return armLength;
    }

    public void setArmLength(float armLength)
    {
        if (armLength > 0)
            this.armLength = armLength;
    }
}
