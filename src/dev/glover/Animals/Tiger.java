package dev.glover.Animals;

public class Tiger extends Mammal
{
    private int stripes;

    Tiger() {}

    Tiger(int i, int stripes)
    {
        super(i);
        setStripes(stripes);
    }

    public int getStripes()
    {
        return stripes;
    }

    public void setStripes(int stripes)
    {
        if (stripes >= 0)
            this.stripes = stripes;
    }
}
