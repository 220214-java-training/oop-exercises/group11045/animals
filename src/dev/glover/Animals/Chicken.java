package dev.glover.Animals;

public class Chicken extends Bird{
    private String featherColor;

    Chicken()
    {
    }

    Chicken(int i, String Color)
    {
        super(i);
        setFeatherColor(Color);
    }

    public String getFeatherColor() {return featherColor;}
    public void setFeatherColor(String featherColor) {this.featherColor = featherColor; }
}
