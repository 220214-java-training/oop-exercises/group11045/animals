package dev.glover.Animals;

public class Driver
{
    public static void main(String[] args)
    {
        Monkey m = new Monkey(2,4.2f);      // Monkey object
        Dog d = new Dog(4, "red");              // Dog object
        Tiger t = new Tiger(4, 25);            // Tiger object
        Chicken c = new Chicken(4, "white");    //Chicken object

        ShowOutput(m, d, t, c);                           // Shows result
    }

    private static void ShowOutput(Monkey m, Dog d, Tiger t, Chicken c)
    {
        // Monkey Output
        System.out.println("");
        System.out.println("This monkey:");
        System.out.println("\tNumber of Legs - " + m.legs);
        System.out.println("\tArm Length - " + m.getArmLength() + "ft.");
        System.out.println("");

        // Dog Output
        System.out.println("");
        System.out.println("This dog:");
        System.out.println("\tNumber of Legs - " + d.legs);
        System.out.println("\tFur Color - " + d.getFurColor());
        System.out.println("");

        // Tiger Output
        System.out.println("");
        System.out.println("This tiger:");
        System.out.println("\tNumber of Legs - " + t.legs);
        System.out.println("\tNumber of Stripes - " + t.getStripes());
        System.out.println("");

        //Chicken output
        System.out.println("");
        System.out.println("This chicken:");
        System.out.println("\tNumber of wings - " + c.wings);
        System.out.println("\tColor of feathers - " + c.getFeatherColor());
        System.out.println("");
    }
}
