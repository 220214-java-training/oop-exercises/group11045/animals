package dev.glover.Animals;

public abstract class Mammal
{
    protected int legs;

    Mammal()
    {
    }

    Mammal(int i)
    {
        if (i > 0)
            this.legs = i;
    }
}
