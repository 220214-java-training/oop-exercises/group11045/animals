package dev.glover.Animals;

public class Dog extends Mammal
{
    private String furColor;

    Dog()
    {
    }

    Dog(int i, String color)
    {
        super(i);
        setFurColor(color);
    }



    public String getFurColor()
    {
        return furColor;
    }

    public void setFurColor(String furColor)
    {
        this.furColor = furColor;
    }
}
